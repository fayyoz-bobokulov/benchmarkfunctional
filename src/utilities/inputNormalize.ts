import * as path from 'path'
import * as os from 'os';
import { Inputs } from '../types/args';

const inputs = {
  pathName: process.argv[2],
  iterations: process.argv[3],
  runs: process.argv[4]
}

function  formatOfHomedir(pathName:string){
  if(pathName.charAt(0)==='~'){
    return true
  } else {
    return false
  }
}

function makingHomedirAbsolutePath(pathName: string){
  pathName = pathName.substring(1)
  pathName = os.homedir().concat(pathName)

  return pathName
}


function makingAbosolutePath(pathName: string){
  return path.resolve(process.cwd(), path.normalize(pathName))
}


function pathNorm(pathName:string) {
  
  if(formatOfHomedir(pathName)){
    pathName = makingHomedirAbsolutePath(pathName)
  } 

  if(!path.isAbsolute(pathName)){
    pathName = makingAbosolutePath(pathName)
  }

  return pathName
  
}

function normIteration(iterations: string){
  return Number(iterations)
}


function normRun(runs: string){
  return Number(runs)  
}

async function normalization(inputs: Inputs){
  
  const normedPath = pathNorm(inputs.pathName)
  const normedIteration = normIteration(inputs.iterations)
  const normedRun = normRun(inputs.runs)

 
  return {

    pathName: normedPath,
    iterations: normedIteration,
    runs: normedRun

  }
}

normalization(inputs)

export {normalization}