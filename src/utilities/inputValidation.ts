import * as fs from 'fs';
import { NormedInputs } from '../types/args'


function fileNameCheck(pathToFileCheck: string){

  return new Promise((resolve, reject) => {
    fs.access(pathToFileCheck, fs.constants.F_OK | fs.constants.R_OK, (err) => {
      if(err){
        throw Error(`Pls check your path of the file: ${pathToFileCheck}`)
      }
    });
  }) 

}

function iterationCheck(iteration: number){

    if(!isNaN(iteration)){
      throw Error (`Pls check your iterations and try again: ${iteration}`)
    }

}

function runCheck(run: number){

  if(!isNaN(run)){
    throw Error (`Pls check your run: ${run}`)
  } 

}

async function inputValidation(inputs: NormedInputs){

  try {

    let checkedFileName = await fileNameCheck(inputs.pathName)

    let checkedIteration = iterationCheck(inputs.iterations)

    let checkedRun = runCheck(inputs.runs)
    
    if(checkedFileName === true && checkedIteration && checkedRun){

      return true

    }
    
  } catch (error) {

    throw error.message
    
  }; 

};

export { inputValidation }
