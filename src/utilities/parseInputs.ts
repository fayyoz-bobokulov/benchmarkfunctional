function parseInputs(inputs: string[]){
  return {
    pathName: inputs[2],
    iterations: inputs[3],
    runs: inputs[4]
  }
}

// console.log(parseInputs(process.argv))

export { parseInputs }

