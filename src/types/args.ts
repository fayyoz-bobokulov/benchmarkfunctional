interface Inputs{
  pathName: string;
  iterations: string;
  runs: string
}

interface NormedInputs{
  pathName: string;
  iterations: number;
  runs: number;
}

export {Inputs, NormedInputs}