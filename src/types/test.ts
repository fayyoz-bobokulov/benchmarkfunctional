
interface ITest {
  name: string;
  func: () => void
}

interface ITestResult {
  testName: string;
  cpu: number;
  memory:number;
  time: number;
}

interface IResultOfRuns {
  time: number;
  memory: number;
  cpu: number;
}

interface IResultOfRunsCPU {
  time: number;
  memory: number;
  cpu: NodeJS.CpuUsage
}

export { ITest, ITestResult, IResultOfRuns, IResultOfRunsCPU }