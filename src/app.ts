import { inputValidation } from './utilities/inputValidation.js';
import { normalization } from './utilities/inputNormalize.js';
import { parseInputs } from './utilities/parseInputs.js';
import { getTestResult } from './benchmark/runTest.js';


(async ()=> {
  try {

    const inputs = parseInputs(process.argv)
    
    const normedInputs = await normalization(inputs)

    inputValidation(normedInputs)

    const tests = (await import(normedInputs.pathName)).default

    const testResult = await getTestResult(tests, normedInputs.iterations, normedInputs.runs)

    // console.log(testResult)

  } catch (error) {

    console.error(error.message)

  }
  
})()


