import { wait } from '../utilities/wait.js';
import { IResultOfRuns, ITest, IResultOfRunsCPU, ITestResult } from '../types/test.js'

function initialValues(){

  let time0 = Number(process.hrtime.bigint());
  let memory0 = process.memoryUsage().heapUsed;
  let cpu0 = process.cpuUsage();

  return {
    time: time0,
    memory: memory0,
    cpu: cpu0
  }

}

const initVal = initialValues()

// console.log(initialValues())

function finishValues(initialValues: IResultOfRunsCPU){

  let time1 = Number(process.hrtime.bigint())
  let memeory1 = process.memoryUsage().heapUsed
  // let { user, system } = process.cpuUsage(initialValues.cpu);
  let cpu1 = process.cpuUsage(initialValues.cpu);

  return {
    time: time1,
    memory: memeory1,
    cpu: cpu1
  }

}

const finVal = finishValues(initVal)

// console.log(finVal)

function getDifferenceResultsOfRuns(initialValues: IResultOfRunsCPU, finishValues: IResultOfRunsCPU){

  let timeDif = finishValues.time - initialValues.time
  let memoryDif = finishValues.memory - initialValues.memory
  let { user, system } = finishValues.cpu;
  return {
    time: timeDif,
    memory: memoryDif,
    cpu: user + system
  }

}
const diff = getDifferenceResultsOfRuns(initVal, finVal)

// console.log(diff)

function getMeanValueOfResult(resultOfRun: IResultOfRuns[], runs: number) {

  let totalTimeSpent = 0;
  let totalMemoryUsage = 0;
  let totalCpuUsage = 0;

  resultOfRun.forEach(result => {

    totalTimeSpent += result.time
    totalMemoryUsage += result.memory
    totalCpuUsage += result.cpu

  })
  
  return {
    
    time: totalTimeSpent/runs,
    memory: totalMemoryUsage/runs,
    cpu: totalCpuUsage/runs

  }
}

async function runTest(tests: ITest, iterations:number, runs: number){

  let results:any = []

  for(let i=0; i<runs; i++){
    
    let initVal = initialValues()

    for (let j = 0; j < iterations; j++) {
      tests.func()
    }

    let finVal = finishValues(initVal)

    let difference = getDifferenceResultsOfRuns(initVal, finVal)

    results.push(difference)

    await wait(400)

  } 
  
  let meanOfRun = getMeanValueOfResult(results, runs)
  
  return {
    testName: tests.name,
    time: meanOfRun.time,
    memory: meanOfRun.memory,
    cpu: meanOfRun.cpu
  }
}

async function getTestResult(tests: ITest[], iterations: number, runs: number ){
  let finalResult = []

  for(let n=0; n<tests.length; n++){

    let resultOfSingleTest = await runTest(tests[n], iterations, runs)

    finalResult.push(resultOfSingleTest);

  }

  console.table(finalResult)
}




export { getTestResult }
